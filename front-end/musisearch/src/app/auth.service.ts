import { Injectable } from '@angular/core';
import { User } from './modele';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}

  // public seConnecter(userInfo: User){
  //   localStorage.setItem('ACCESS_TOKEN', "access_token");
  // }
  // public estConnecte(){
  //   return localStorage.getItem('ACCESS_TOKEN') !== null;
  // }
  
  // public deconnecter(){
  //   localStorage.removeItem('ACCESS_TOKEN');
  // }

  login(mail: string, mdp: string): void {
    this.http.post(`${environment.baseUrl}/api/login_check`, { mail, mdp })
         .pipe(
             map(response => {
                 // login successful if there's a jwt token in the response
                 if (response) {
                   localStorage.setItem('jwt', JSON.stringify(response));
                 }
             })
         );
 }

}
