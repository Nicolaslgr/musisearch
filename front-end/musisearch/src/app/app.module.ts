import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { AccueilsansconnexionComponent } from './accueilsansconnexion/accueilsansconnexion.component';
import { AppRoutingModule } from './app-routing.module';
import { InscriptionComponent } from './inscription/inscription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccueilComponent } from './accueil/accueil.component';
import { JwtInterceptor } from './jwtInterceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BarremusiqueComponent } from './accueil/barremusique/barremusique.component';
import { ProfilComponent } from './accueil/profil/profil.component';


@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    AccueilsansconnexionComponent,
    InscriptionComponent,
    AccueilComponent,
    BarremusiqueComponent,
    ProfilComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
],
  bootstrap: [AppComponent]
})
export class AppModule { }
