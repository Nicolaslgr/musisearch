import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueilsansconnexion',
  templateUrl: './accueilsansconnexion.component.html',
  styleUrls: ['./accueilsansconnexion.component.scss']
})
export class AccueilsansconnexionComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

}
